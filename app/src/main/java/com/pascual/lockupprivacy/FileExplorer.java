package com.pascual.lockupprivacy;

import android.content.Intent;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;

public class FileExplorer extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private List<String> nombresArchivos;
    private List<Integer> imagenesArchivos;
    private List<String> rutasArchivos;
    private String directorioRaiz;
    private TextView carpetaActual;
    String sNombreArchivoIndicado;
    String sRutaSinArchivo = "";
    ListView listas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_explorer);
        utils.setNotificationBar(this);

        Button btnRegresaRuta = findViewById(R.id.btnRegresarRuta);


        carpetaActual = findViewById(R.id.lblRutaActual);
        listas = findViewById(R.id.lvLista);

        directorioRaiz = Environment.getExternalStorageDirectory().getPath();
        listas.setOnItemClickListener(this);
        verDirectorio(directorioRaiz);


        btnRegresaRuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent backButtonIntent;
                backButtonIntent = new Intent(getApplicationContext(), Encrypted_files.class);
                startActivity(backButtonIntent);
                FileExplorer.this.finish();
            }
        });

    }


    /**
     * Metodo que nos permite ver el directorio de la ruta especificada
     */
    private void verDirectorio(String rutaDirectorio) {
        nombresArchivos = new ArrayList<String>();
        imagenesArchivos = new ArrayList<Integer>();
        rutasArchivos = new ArrayList<String>();
        int count = 0;

        File directorioActual = new File(rutaDirectorio);
        File[] listasArchivos = directorioActual.listFiles();

        if (!rutaDirectorio.equals(directorioRaiz)) {
            nombresArchivos.add("");// en caso de que queiran poner los ... aqui se puede hacer
            imagenesArchivos.add(R.drawable.points);
            //getParent devuelve el nombre de la ruta de los padres de este archivo
            rutasArchivos.add(directorioActual.getParent());
            count = 1;
        }

        // Almacenamos las rutas de todos los archivos y carpetsa del directorio
        for (File archivo : listasArchivos) {
            rutasArchivos.add(archivo.getPath());
        }

        // Ordenamos la lista de los archivos para que se muestren en orden alfavetico
        Collections.sort(rutasArchivos, String.CASE_INSENSITIVE_ORDER);

        //Recorremos la lista de archivos ordenada para crear la lista de los nombrs de los archivos que mostramos en el listview
        for (int i = count; i < rutasArchivos.size(); i++) {
            File archivo = new File(rutasArchivos.get(i));
            // si es archivo (isFile) lo indicara con un true de otra forma no lo es
            if (archivo.isFile()) {
                nombresArchivos.add(archivo.getName());
                String[] sval;
                String nombre = nombresArchivos.get(i);

                //     String s = "123-456";
                sval = nombre.split("\\.");

                if (sval.length == 2) {
                    if (sval[1].equals("png") || sval[1].equals("jpg") || sval[1].equals("jpeg") || sval[1].equals("img") || sval[1].equals("icon")) {
                        imagenesArchivos.add(R.drawable.image);
                    } else if (sval[1].equals("doc") || sval[1].equals("txt")) {
                        imagenesArchivos.add(R.drawable.texto);
                    } else if (sval[1].equals("xlsx")) {
                        imagenesArchivos.add(R.drawable.excel);
                    } else if (sval[1].equals("pdf")) {
                        imagenesArchivos.add(R.drawable.pdf);
                    } else if (sval[1].equals("mp3")) {
                        imagenesArchivos.add(R.drawable.mp3);
                    } else if (sval[1].equals("mp4")) {
                        imagenesArchivos.add(R.drawable.mp4);
                    } else if (sval[1].equals("rar") || sval[1].equals("zip")) {
                        imagenesArchivos.add(R.drawable.rar);
                    } else if (sval[1].equals("exe")) {
                        imagenesArchivos.add(R.drawable.excec);
                    }  else if (sval[1].equals("apk")) {
                        imagenesArchivos.add(R.drawable.apkicon);
                    } else {
                        imagenesArchivos.add(R.drawable.question);
                    }
                } else if (sval.length == 3) {
                    if (sval[2].equals("png") || sval[2].equals("jpg") || sval[2].equals("jpeg") || sval[2].equals("img") || sval[2].equals("icon")) {
                        imagenesArchivos.add(R.drawable.image);
                    } else if (sval[2].equals("doc") || sval[1].equals("txt")) {
                        imagenesArchivos.add(R.drawable.texto);
                    } else if (sval[2].equals("xlsx")) {
                        imagenesArchivos.add(R.drawable.excel);
                    } else if (sval[2].equals("pdf")) {
                        imagenesArchivos.add(R.drawable.pdf);
                    } else if (sval[2].equals("mp3")) {
                        imagenesArchivos.add(R.drawable.mp3);
                    } else if (sval[2].equals("mp4")) {
                        imagenesArchivos.add(R.drawable.mp4);
                    } else if (sval[2].equals("rar") || sval[2].equals("zip")) {
                        imagenesArchivos.add(R.drawable.rar);
                    } else if (sval[2].equals("exe")) {
                        imagenesArchivos.add(R.drawable.excec);
                    } else if (sval[2].equals("apk")) {
                        imagenesArchivos.add(R.drawable.apkicon);
                    }  else {
                        imagenesArchivos.add(R.drawable.question);
                    }
                } else {
                    imagenesArchivos.add(R.drawable.question);

                }

            } else {
                nombresArchivos.add("/" + archivo.getName());
                imagenesArchivos.add(R.drawable.folder);
            }
        }

        // Si no hay ningun archivo en el directorio lo indicamos
        if (listasArchivos.length < 1) {
            nombresArchivos.add("No hay archivos");
            imagenesArchivos.add(R.drawable.empty);
            rutasArchivos.add(rutaDirectorio);
        }
        //Creamos el adaptador y le asignamos la lista de los nombres de los archivos  y el layout para los elementos de la lista

        listas.setAdapter(new List_Adapter(this, nombresArchivos, imagenesArchivos));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        // Obtenemos la ruta del archivo en el que hemos hecho click en el listview
        File archivo = new File(rutasArchivos.get(i));
        //Si es un archivo se muestra un Toast con su nombre y si es un directorio se cargan los archivos que continee el listview
        if (archivo.isFile()) {

            String[] sval;
            String nombre = archivo.getName();

            //     String s = "123-456";
            sval = nombre.split("\\.");

            if (sval.length == 2) {
                if (sval[1].equals("png") || sval[1].equals("jpg") || sval[1].equals("jpeg") || sval[1].equals("img") || sval[1].equals("icon")) {

                    sNombreArchivoIndicado = archivo.getName();
                    Intent intent = new Intent(this, ShowImage.class);

                    String[] parts = rutasArchivos.get(i).split("/");

                    sRutaSinArchivo = parts[parts.length - 2];

                    intent.putExtra("ruta", archivo.toString());
                    intent.putExtra("archivo", sNombreArchivoIndicado);
                    intent.putExtra("rutasin", sRutaSinArchivo);
                    intent.putExtra("bandera", "luis");
                    startActivity(intent);

                    FileExplorer.this.finish();
                }else{
                    Toast.makeText(this, "Este tipo de archivo no se puede encriptar", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            //Si es un directorio mostramos todos los archivos que contiene
            verDirectorio(rutasArchivos.get(i));
        }
    }
    @Override
    protected void onStop() {
        this.finish(); super.onStop();

    }

    @Override
    public void onBackPressed() {
        Intent backIntent = new Intent(this, Encrypted_files.class);
        startActivity(backIntent);
        FileExplorer.this.finish();
    }

}

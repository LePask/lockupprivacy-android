package com.pascual.lockupprivacy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Encrypted_files extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private List<String> nombresArchivos;
    private List<Integer> imagenesArchivos;
    private List<String> rutasArchivos;
    private ArrayAdapter<String> adapter;
    private String directorioRaiz;
    private TextView carpetaActual;
    String sNombreArchivoIndicado;
    String sRutaSinArchivo = "";
    ListView listas;
    public Button btnRegresaEncr;
    public ImageButton btnEncripta;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encrypted_files);
        utils.setNotificationBar(this);
        creationOfFolderInInternalStorage();

        btnRegresaEncr = findViewById(R.id.btnRegresarEncr);
        btnEncripta=findViewById(R.id.btnEncriptar);

        btnRegresaEncr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

          onBackPressed();
            }
        });

        btnEncripta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),FileExplorer.class);
                startActivity(intent);
            }
        });


        carpetaActual = findViewById(R.id.lblRutaActual);
        listas = findViewById(R.id.lvLista);

        directorioRaiz = Environment.getExternalStorageDirectory().getPath();
        listas.setOnItemClickListener(this);
        verDirectorio(directorioRaiz);
    }

    private void verDirectorio(String rutaDirectorio) {
        nombresArchivos = new ArrayList<String>();
        imagenesArchivos = new ArrayList<Integer>();
        rutasArchivos = new ArrayList<String>();
        int count = 0;
        rutaDirectorio = rutaDirectorio + "/Encriptadas";
        File directorioActual = new File(rutaDirectorio);
        File[] listasArchivos = directorioActual.listFiles();


        // Almacenamos las rutas de todos los archivos y carpetsa del directorio
        for (File archivo : listasArchivos) {
            rutasArchivos.add(archivo.getPath()  /*+ "Encriptadas"*/);
        }

        // Ordenamos la lista de los archivos para que se muestren en orden alfavetico
        Collections.sort(rutasArchivos, String.CASE_INSENSITIVE_ORDER);

        //Recorremos la lista de archivos ordenada para crear la lista de los nombrs de los archivos que mostramos en el listview
        for (int i = count; i < rutasArchivos.size(); i++) {
            File archivo = new File(rutasArchivos.get(i));
            // si es archivo (isFile) lo indicara con un true de otra forma no lo es
            if (archivo.isFile()) {
                nombresArchivos.add(archivo.getName());
                imagenesArchivos.add(R.drawable.encrypt);
            } else {
                nombresArchivos.add("/" + archivo.getName());
                imagenesArchivos.add(R.drawable.folder);
            }
        }

        // Si no hay ningun archivo en el directorio lo indicamos
        if (listasArchivos.length < 1) {
            nombresArchivos.add("No hay archivos");
            imagenesArchivos.add(R.drawable.empty);
            rutasArchivos.add(rutaDirectorio);
        }
        //Creamos el adaptador y le asignamos la lista de los nombres de los archivos  y el layout para los elementos de la lista
        //adapter = new ArrayAdapter<String>(this, R.layout.file_list, nombresArchivos);
        //listas.setAdapter(adapter);
        listas.setAdapter(new List_Adapter(this, nombresArchivos, imagenesArchivos));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        // Obtenemos la ruta del archivo en el que hemos hecho click en el listview
        File archivo = new File(rutasArchivos.get(i));
        //Si es un archivo se muestra un Toast con su nombre y si es un directorio se cargan los archivos que continee el listview
        if (archivo.isFile()) {

            sNombreArchivoIndicado = archivo.getName();
            Intent intent = new Intent(this, ShowImage.class);

            String[] parts = rutasArchivos.get(i).split("/");

            sRutaSinArchivo = parts[parts.length - 2];

            intent.putExtra("ruta", archivo.toString());
            intent.putExtra("archivo", sNombreArchivoIndicado);
            intent.putExtra("rutasin", sRutaSinArchivo);
            intent.putExtra("bandera", "leo");
            startActivity(intent);
            this.finish();


        } else {
            //Si es un directorio mostramos todos los archivos que contiene
            verDirectorio(rutasArchivos.get(i));
        }
    }


    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Pantalla de confirmación");

        builder.setMessage("¿Seguro qué deseas salir de la aplicación?")
                .setCancelable(false)
                .setPositiveButton("Si, estoy seguro", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            this.finalize();
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                        System.exit(0);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
    @Override
    protected void onStop() {
        this.finish(); super.onStop();


    }

    public void btnEncriptarArchivo(View view) {
        Intent intent = new Intent(this, FileExplorer.class);
        startActivity(intent);
        Encrypted_files.this.finish();
    }

    private void creationOfFolderInInternalStorage() {
        File myDir;
        String folder;
        folder = "Encriptadas";
        myDir = new File(Environment.getExternalStorageDirectory(), folder);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
    }

}

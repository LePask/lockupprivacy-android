package com.pascual.lockupprivacy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

public class ShowImage extends AppCompatActivity {
    TextView lblRuta, lblArchivo, lblRutaSinArchivo;
    ImageView imvImagen;
    Button btnEncrypt, btnDecrypt, btnRegresar;

    File myDir;
    File myDirOri;

    String sImgEnc;
    String sImgDec;
    String ruta;
    String nombreArchivo;
    String rutaSinArchivo;
    String sBandera;
    File file;
    String[] separador;
    String[] separawolf;
    String sRutasAnteriores = "";

    //KEY - Hardcode in code
    //In realife you can save it on Firebase / API and get when runtime
    String my_key = "Vcs8lcNuJPGOHwl1"; //16 char = 128 bits
    String my_spec_key = "UAJf5XWFAnh3mK9u";

    @Override
    protected void onStop() {
        this.finish(); super.onStop();


    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);
        utils.setNotificationBar(this);

        lblRuta = findViewById(R.id.lblRuta);
        lblArchivo = findViewById(R.id.lblNombreArchivo);
        lblRutaSinArchivo = findViewById(R.id.lblRutaSinArchivo);
        imvImagen = findViewById(R.id.imageView);
        btnEncrypt = findViewById(R.id.btnEncrypt);
        btnDecrypt = findViewById(R.id.btnDecrypt);
        btnRegresar = findViewById(R.id.btnRegresarIma);

        ruta = getIntent().getStringExtra("ruta");
        nombreArchivo = getIntent().getStringExtra("archivo");
        rutaSinArchivo = getIntent().getStringExtra("rutasin");
        sBandera = getIntent().getStringExtra("bandera");

        separawolf = ruta.split("/");

        boolean bDesdeAqui = false;
        for (int i = 0; i < separawolf.length - 1; i++) {
            if (bDesdeAqui == true) {
                sRutasAnteriores = sRutasAnteriores + "%" + separawolf[i];
            }
            if (separawolf[i].equals("0")) {
                bDesdeAqui = true;
            }

        }
        lblRuta.setText(ruta);
        lblArchivo.setText(nombreArchivo);
        lblRutaSinArchivo.setText(rutaSinArchivo);

        file = new File(ruta);

        //Init path de archivos encriptados
        nombreArchivo = nombreArchivo + sRutasAnteriores;
        separador = nombreArchivo.split("%");

        sImgEnc = nombreArchivo;
        sImgDec = separador[0];

        if (sBandera.equals("luis")) {
            if (file.toString().contains(".pdf") || file.toString().contains(".docx") || file.toString().contains(".doc") || file.toString().contains(".txt")) {
                imvImagen.setImageResource(R.drawable.texto);
                btnDecrypt.setEnabled(false);
                btnEncrypt.setEnabled(true);
            } else if (file.toString().contains(".ppt") || file.toString().contains(".pptx")) {
                imvImagen.setImageResource(R.drawable.pres);
                btnDecrypt.setEnabled(false);
                btnEncrypt.setEnabled(true);
            } else if (file.toString().contains(".xls") || file.toString().contains(".xlsx")) {
                imvImagen.setImageResource(R.drawable.excel);
                btnDecrypt.setEnabled(false);
                btnEncrypt.setEnabled(true);
            } else if (file.toString().contains(".wav") || file.toString().contains(".mp3")) {
                imvImagen.setImageResource(R.drawable.mp3);
                btnDecrypt.setEnabled(false);
                btnEncrypt.setEnabled(true);
            } else if (file.toString().contains(".mp4") || file.toString().contains(".avi")) {
                imvImagen.setImageResource(R.drawable.mp4);
                btnDecrypt.setEnabled(false);
                btnEncrypt.setEnabled(true);
            } else {
                imvImagen.setImageURI(Uri.fromFile(file));
                btnDecrypt.setEnabled(false);
                btnDecrypt.setVisibility(View.INVISIBLE);
                btnEncrypt.setEnabled(true);
                btnEncrypt.setVisibility(View.VISIBLE);
            }
        } else {
            if (file.toString().contains(".pdf") || file.toString().contains(".docx") || file.toString().contains(".doc") || file.toString().contains(".txt")) {
                imvImagen.setImageResource(R.drawable.texto);
                btnDecrypt.setEnabled(true);
                btnEncrypt.setEnabled(false);
            } else if (file.toString().contains(".ppt") || file.toString().contains(".pptx")) {
                imvImagen.setImageResource(R.drawable.pres);
                btnDecrypt.setEnabled(true);
                btnEncrypt.setEnabled(false);
            } else if (file.toString().contains(".xls") || file.toString().contains(".xlsx")) {
                imvImagen.setImageResource(R.drawable.excel);
                btnDecrypt.setEnabled(true);
                btnEncrypt.setEnabled(false);
            } else if (file.toString().contains(".wav") || file.toString().contains(".mp3")) {
                imvImagen.setImageResource(R.drawable.mp3);
                btnDecrypt.setEnabled(true);
                btnEncrypt.setEnabled(false);
            } else if (file.toString().contains(".mp4") || file.toString().contains(".avi")) {
                imvImagen.setImageResource(R.drawable.mp4);
                btnDecrypt.setEnabled(true);
                btnEncrypt.setEnabled(false);
            } else {
                imvImagen.setImageResource(R.drawable.encrypt);
                btnDecrypt.setEnabled(true);
                btnDecrypt.setVisibility(View.VISIBLE);
                btnEncrypt.setEnabled(false);
                btnEncrypt.setVisibility(View.INVISIBLE);
            }
        }


        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog pdElemento = new ProgressDialog(ShowImage.this);
                pdElemento.setMessage("Cargando..");
                pdElemento.show();

                //Convertir Drawable en bitmap
                Drawable drawable = imvImagen.getDrawable();
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                Bitmap bitmap = bitmapDrawable.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                InputStream is = new ByteArrayInputStream(stream.toByteArray());

                if (!separador[1].equals("Encriptadas")) {
                    separador[1] = "Encriptadas";
                    myDir = new File(Environment.getExternalStorageDirectory(), separador[1]);
                }

                //Create file
                File outputFileEnc = new File(myDir, sImgEnc);

                //Borrar la imagen de la ubicacion original
                ruta = lblRuta.getText().toString();

                btnDecrypt.setEnabled(true);
                btnEncrypt.setEnabled(false);
                btnEncrypt.setVisibility(View.INVISIBLE);
                btnDecrypt.setVisibility(View.VISIBLE);

                File fdelete = new File(ruta);
                if (fdelete.exists()) {
                    if (fdelete.delete()) {
                        Toast.makeText(ShowImage.this, "Eliminamos el rastro.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ShowImage.this, "No se pudo eliminar el rastro.", Toast.LENGTH_SHORT).show();
                    }
                }

                try {
                    MyEncrypter.encryptToFile(my_key, my_spec_key, is, new FileOutputStream(outputFileEnc));

                    Toast.makeText(ShowImage.this, "Se ha encriptado exitosamente!", Toast.LENGTH_SHORT).show();
                    btnEncrypt.setEnabled(false);
                    pdElemento.hide();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                } catch (InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                }



                ShowImage.this.finish();
                Intent intent = new Intent(getApplicationContext(), Encrypted_files.class);
                startActivity(intent);
            }
        });

        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog pdElemento = new ProgressDialog(ShowImage.this);
                pdElemento.setMessage("Cargando..");
                pdElemento.show();
                if (separador[2].equals("Encriptadas")) {

                    myDirOri = new File(Environment.getExternalStorageDirectory(), separador[1]);
                }
                String sRutaDeRegreso = "";
                for (int i = 1; i < separador.length - 1; i++) {
                    sRutaDeRegreso = sRutaDeRegreso + "/" + separador[i];
                }
                myDir = new File(Environment.getExternalStorageDirectory(), separador[separador.length - 1]);
                myDirOri = new File(Environment.getExternalStorageDirectory(), sRutaDeRegreso);

                File outputFileDec = new File(myDirOri, sImgDec);

                sImgEnc = "";
                for (int j = 0; j < separador.length - 1; j++) {
                    if (j == 0) {
                        sImgEnc = separador[j];
                    } else {
                        sImgEnc = sImgEnc + "%" + separador[j];
                    }
                }
                File encFile = new File(myDir, sImgEnc);

                try {

                    MyEncrypter.decryptToFile(my_key, my_spec_key, new FileInputStream(encFile),
                            new FileOutputStream(outputFileDec));

                    //set for image view
                    imvImagen.setImageURI(Uri.fromFile(outputFileDec));

                    lblRuta.setText(outputFileDec.getPath());
                    lblArchivo.setText(sImgDec);
                    lblRutaSinArchivo.setText(separador[1]);

                    Toast.makeText(ShowImage.this, "Se ha desencriptado exitosamente!", Toast.LENGTH_SHORT).show();

                    //Borrar el archivo despues de descriptar
                    encFile.delete();

                    btnEncrypt.setEnabled(true);
                    btnDecrypt.setEnabled(false);
                    btnDecrypt.setVisibility(View.INVISIBLE);
                    btnEncrypt.setVisibility(View.VISIBLE);
                    pdElemento.hide();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                } catch (InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sBandera.equals("luis")) {
                    Intent intent = new Intent(getApplicationContext(), FileExplorer.class);
                    startActivity(intent);

                    ShowImage.this.finish();
                } else {
                    Intent intent = new Intent(getApplicationContext(), Encrypted_files.class);
                    startActivity(intent);

                    ShowImage.this.finish();
                }

            }
        });

    }


    @Override
    public void onBackPressed() {
        if (sBandera.equals("luis")) {
            Intent intent = new Intent(getApplicationContext(), FileExplorer.class);
            startActivity(intent);

            ShowImage.this.finish();
        } else {
            Intent intent = new Intent(getApplicationContext(), Encrypted_files.class);
            startActivity(intent);

            ShowImage.this.finish();
        }
    }


}

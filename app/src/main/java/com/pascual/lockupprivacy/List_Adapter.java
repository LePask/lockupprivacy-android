package com.pascual.lockupprivacy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class List_Adapter  extends BaseAdapter {
    private LayoutInflater inflater = null;

    Context context;
     List<String> nombresArchivos;
     List<Integer> imagenesArchivos;


    public List_Adapter(Context context, List<String> nombresArchivos, List<Integer> imagenesArchivos) {
        this.context = context;
        this.nombresArchivos = nombresArchivos;
        this.imagenesArchivos = imagenesArchivos;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imagenesArchivos.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final View vista = inflater.inflate(R.layout.adaptador_lista,null);

        TextView ruta = vista.findViewById(R.id.lblRutaAdaptador);
        ImageView imagen = vista.findViewById(R.id.lblImagenAdaptador);
        ruta.setText(nombresArchivos.get(i));
        imagen.setImageResource(imagenesArchivos.get(i));

        return vista;
    }
}

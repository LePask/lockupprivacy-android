package com.pascual.lockupprivacy;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import static android.support.v4.content.ContextCompat.startActivity;

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
    Intent intent;
    private Context context;

    public FingerprintHandler(Context context) {
        this.context = context;
    }

    public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        this.update("Hubo un error en la autenticación. " + errString, false);
    }

    @Override
    public void onAuthenticationFailed() {
        this.update("La autenticación ha fallado. ", false);
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        this.update("Error: " + helpString, false);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

        // Cambiamos el texto que se muestra a permitido
        this.update("Acceso a la app permitido.", true);
        intent = new Intent(this.context,Encrypted_files.class);

        /**
         * Usamos un timer para tardarnos dos segundos en abrir la aplicacion y que no sea de
         * una forma repentina, posteriormente lanzamos una nueva ventana con un intento y
         * despues cerramos esta ventana para al momento de regresar, evitar un bug, por lo que
         * al regresar tendremos que abrir una nueva ventana.
         */
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(context,intent,null);
                MainActivity.activity.finish();
            }
        },750);


    }

    private void update(String s, boolean b) {
        TextView paraLabel = (TextView) ((Activity) context).findViewById(R.id.tvText);
        ImageView imageView = (ImageView) ((Activity) context).findViewById(R.id.ivFingerprint);


        if (b == false) {
            paraLabel.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            imageView.setImageResource(R.mipmap.cancel_main_icon);
        } else {
            paraLabel.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            imageView.setImageResource(R.mipmap.check_main_icon);
            paraLabel.setText(s);
        }
    }
}
